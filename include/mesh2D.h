#define iint int
#define dfloat float
#define MPI_IINT MPI_INT
#define MPI_DFLOAT MPI_FLOAT
#define iintFormat "%d"

typedef struct {

  iint Nverts, Nfaces;

  iint Nnodes;
  dfloat *VX;
  dfloat *VY;
  
  iint Nelements;
  iint *EToV;
  iint *EToE;
  iint *EToF;
  iint *EToP;
}mesh2D;

mesh2D* meshReader2D(char *fileName);
mesh2D* meshParallelReader2D(char *fileName);

void meshConnect2D(mesh2D *mesh);
void meshParallelConnect2D(mesh2D *mesh);

void meshPrint2D(mesh2D *mesh);
void meshParallelPrint2D(mesh2D *mesh);

void meshVTU2D(mesh2D *mesh, char *fileName);

#define mymax(a,b) ((a>b)?a:b)
#define mymin(a,b) ((a<b)?a:b)

void parallelSort(iint N, void *vv, size_t sz,
		  int (*compare)(const void *, const void *),
		  void (*match)(void *, void *)
		  );
  
