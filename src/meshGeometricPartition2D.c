#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "mesh2D.h"

#define bitRange 30

// spread bits of i by introducing zeros between binary bits
unsigned long long int bitSplitter(unsigned int i){
  
  unsigned long long int one = 1;
  unsigned long long int li = i;
  unsigned long long int lj = 0;
  
  for(int b=0;b<bitRange;++b){
    lj |=  (li & ( one << b)) <<b;
  }
  
  return lj;

}

unsigned long long int mortonIndex2D(unsigned int ix, unsigned int iy){
  
  // spread bits of ix apart (introduce zeros)
  unsigned long long int sx = bitSplitter(ix);
  unsigned long long int sy = bitSplitter(iy);

  // interleave bits of ix and iy
  unsigned long long int mi = sx | (sy<<1);
  
  return mi;
}

typedef struct {
  
  unsigned long long int index;
  
  iint element;
  iint v[4];

}element_t;

int compareElements(const void *a, const void *b){

  element_t *ea = (element_t*) a;
  element_t *eb = (element_t*) b;
  
  if(ea->index < eb->index) return -1;
  if(ea->index > eb->index) return  1;
  
  return 0;

}

void bogusMatch(void *a, void *b){ }

void meshGeometricPartition2D(mesh2D *mesh){

  iint maxNelements;
  MPI_Allreduce(&(mesh->Nelements), &maxNelements, 1, MPI_IINT, MPI_MAX,
		MPI_COMM_WORLD);
  maxNelements = 2*((maxNelements+1)/2);
  
  // fix maxNelements
  element_t *elements 
    = (element_t*) calloc(maxNelements, sizeof(element_t));

  // bounding box
  dfloat mincx = 1e9, maxcx = -1e9;
  dfloat mincy = 1e9, maxcy = -1e9;

  // element centers
  for(iint e=0;e<mesh->Nelements;++e){
    dfloat cx = 0, cy = 0;
    for(iint n=0;n<mesh->Nverts;++n){
      cx += mesh->VX[mesh->EToV[e*mesh->Nverts+n]];
      cy += mesh->VY[mesh->EToV[e*mesh->Nverts+n]];
    }
    cx /= mesh->Nverts;
    cy /= mesh->Nverts;
    
    mincx = mymin(mincx, cx);
    maxcx = mymax(maxcx, cx);
    mincy = mymin(mincy, cy);
    maxcy = mymax(maxcy, cy);
  }

  dfloat gmincx, gmincy, gmaxcx, gmaxcy;
  MPI_Allreduce(&mincx, &gmincx, 1, MPI_DFLOAT, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(&mincy, &gmincy, 1, MPI_DFLOAT, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(&maxcx, &gmaxcx, 1, MPI_DFLOAT, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(&maxcy, &gmaxcy, 1, MPI_DFLOAT, MPI_MAX, MPI_COMM_WORLD);

  unsigned long long int Nboxes = (((long long int)1)<<(bitRange-1));

  for(iint e=0;e<mesh->Nelements;++e){

    dfloat cx = 0, cy = 0;
    for(iint n=0;n<mesh->Nverts;++n){
      cx += mesh->VX[mesh->EToV[e*mesh->Nverts+n]];
      cy += mesh->VY[mesh->EToV[e*mesh->Nverts+n]];
    }
    cx /= mesh->Nverts;
    cy /= mesh->Nverts;

    elements[e].element = e;
    for(iint n=0;n<mesh->Nverts;++n)
      elements[e].v[n] = mesh->EToV[e*mesh->Nverts+n];

    unsigned long long int ix = (cx-gmincx)*Nboxes/(gmaxcx-gmincx);
    unsigned long long int iy = (cy-gmincy)*Nboxes/(gmaxcy-gmincy);
			
    elements[e].index = mortonIndex2D(ix, iy);
  }
  for(iint e=mesh->Nelements;e<maxNelements;++e){
    elements[e].element = -1;
    elements[e].index = mortonIndex2D(Nboxes+1, Nboxes+1);
  }

  parallelSort(maxNelements, elements, sizeof(element_t),
	       compareElements, 
	       bogusMatch);

  iint cnt = 0;
  for(iint e=0;e<maxNelements;++e)
    cnt += (elements[e].element != -1);

  free(mesh->EToV);
  mesh->Nelements = cnt;
  mesh->EToV = (iint*) calloc(cnt*mesh->Nverts, sizeof(iint));
  cnt = 0;
  for(iint e=0;e<maxNelements;++e){
    if(elements[e].element != -1){
      for(iint n=0;n<mesh->Nverts;++n){
	mesh->EToV[cnt*mesh->Nverts + n] = 
	  elements[e].v[n];
      }
      ++cnt;
    }
  }
}
