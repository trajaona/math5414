#include "mpi.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mesh2D.h"

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);

  mesh2D *mesh;

  mesh = meshParallelReader2D(argv[1]);

  void meshGeometricPartition2D(mesh2D *mesh);
  meshGeometricPartition2D(mesh);
  
  meshParallelConnect2D(mesh);


  
  meshParallelPrint2D(mesh);

  // create vtu file name
  char vtuName[BUFSIZ];
  sprintf(vtuName, "%s.vtu", strtok(argv[1], "."));
  meshVTU2D(mesh, vtuName);

  MPI_Finalize();

  return 0;
}
